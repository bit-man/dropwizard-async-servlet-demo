package async.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

/**
 * @author Scott Kidder
 */
@Path("/rest")
public class AsyncResource {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public void get(@Suspended final AsyncResponse response) {
		List<String> history = new ArrayList<>();
		history.add("Before-" + Thread.currentThread().getId());
		new Thread(getTarget(response, history)).start();
		history.add("After-" + Thread.currentThread().getId());
	}

	private Runnable getTarget(AsyncResponse response, List<String> history) {
		return new Runnable() {
			public void run() {
				history.add("run-" + Thread.currentThread().getId());
				// sleep for 10 seconds
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				StringBuilder strResponse = new StringBuilder("Hello World!");
				for (String str : history) {
					strResponse.append(", " + str);
				}

				response.resume(strResponse.toString());
			}
		};
	}
}
